# Copyright (C) 2012 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
require 'rubygems'
require 'google/api_client'
require 'google/api_client/client_secrets'
require 'google/api_client/auth/file_storage'
require 'google/api_client/auth/installed_app'
require 'logger'
require 'open3'
require 'pathname'

API_VERSION = 'v2'
CACHED_API_FILE = "drive-#{API_VERSION}.cache"
CREDENTIAL_STORE_FILE = "#{$0}-oauth2.json"


# Handles authentication and loading of the API.
def setup()
  log_file = File.open('drive.log', 'a+')
  log_file.sync = true
  logger = Logger.new(log_file)
  logger.level = Logger::DEBUG

  client = Google::APIClient.new(:application_name => 'Ruby Drive sample',
      :application_version => '1.0.0')

  # FileStorage stores auth credentials in a file, so they survive multiple runs
  # of the application. This avoids prompting the user for authorization every
  # time the access token expires, by remembering the refresh token.
  # Note: FileStorage is not suitable for multi-user applications.
  file_storage = Google::APIClient::FileStorage.new(CREDENTIAL_STORE_FILE)
  if file_storage.authorization.nil?
    client_secrets = Google::APIClient::ClientSecrets.load
    # The InstalledAppFlow is a helper class to handle the OAuth 2.0 installed
    # application flow, which ties in with FileStorage to store credentials
    # between runs.
    flow = Google::APIClient::InstalledAppFlow.new(
      :client_id => client_secrets.client_id,
      :client_secret => client_secrets.client_secret,
      :scope => ['https://www.googleapis.com/auth/drive']
    )
    client.authorization = flow.authorize(file_storage)
  else
    client.authorization = file_storage.authorization
  end

  drive = nil
  # Load cached discovered API, if it exists. This prevents retrieving the
  # discovery document on every run, saving a round-trip to API servers.
  if File.exists? CACHED_API_FILE
    File.open(CACHED_API_FILE) do |file|
      drive = Marshal.load(file)
    end
  else
    drive = client.discovered_api('drive', API_VERSION)
    File.open(CACHED_API_FILE, 'w') do |file|
      Marshal.dump(drive, file)
    end
  end

  results = client.execute!(
    :api_method => drive.files.list,
    :parameters => { 'q' => "mimeType = 'application/vnd.google-apps.folder' and title = 'Google Photos'" })
  googlePhotos = results.data.items[0].id
  puts("Google Photos folder id: " + googlePhotos)

  return client, drive, googlePhotos
end

# Handles files.insert call to Drive API.
def insert_file(client, drive, googlePhotos, f, name, mimeType)
  # Insert a file
  file = drive.files.insert.request_schema.new({
    'title' => name,
    #'description' => 'A test document',
    'mimeType' => mimeType,
    'modifiedDate' => File.mtime(f).strftime("%FT%T.%L%:z"),
    'parents' => [{
      "kind" => "drive#fileLink",
      "id" => googlePhotos
    }]
  })

  media = Google::APIClient::UploadIO.new(f, 'text/plain')
  result = client.execute(
    :api_method => drive.files.insert,
    :body_object => file,
    :media => media,
    :parameters => {
      'uploadType' => 'multipart',
      'alt' => 'json'})

  if (result.data.to_hash.has_key?("error"))
    puts (" Error: " + result.data.error["message"])
    return false
    # Pretty print the API result
    #jj result.data.to_hash
  end

  return true
end

$smugline_prog = "~/dev/smugline/smugline.py"
$smugline_auth = "--api-key=CkrdQM5RSPxYcAaMlWT5F0kuzYUAaHSv --email=ariscris@gmail.com --password=arisgo"

def log(s)
  $al.info(s)
  puts(s)
end

def process_albums()
  log_file = File.open('albums.log', 'a+')
  log_file.sync = true
  $al = Logger.new(log_file)

  cmd = $smugline_prog + " list " + $smugline_auth

  log("Executing: " + cmd)
  list, st = Open3.capture2(cmd)

  skippedFirst = true
  skipUntil = "Palarong Pinoy '09"
  list.each_line do |line|
    album = line.strip()
    if skipUntil != ""
      if album == skipUntil
        skipUntil = ""
      else
        log("skipping " + album)
        next
      end
    end
    if skippedFirst
      process_album(album) 
      #break
    else
      skippedFirst = true
    end
  end
end

def process_album(album)
  if album == "iphone" || album == "PhotoSync" || album == "Photosync 2" || album == "Camera Awesome Archive" ||
    album == "My Gehls Archive" || album.include?("Months Old") || album.include?("years old") || album == "Videos"
    return
  end

  path = "albumfiles"
  log(Time.now.strftime("%d/%m/%Y %H:%M") + " Processing album: " + album)

  Pathname.new(path).children.each { |p| p.unlink }

  cmd = $smugline_prog + " download '" + album + "' --to=" + path + " " + $smugline_auth

  #log(" Executing: " + cmd)
  list, st = Open3.capture2(cmd)

  successCount = 0
  skippedCount = 0
  totalSize = 0
  Dir.entries(path).select do |f| 
    ret, size = process_file(path, f, album)
    if ret == "success"
      successCount = successCount + 1
      totalSize = totalSize + size 
    elsif ret == "skipped"
      skippedCount = skippedCount + 1
    end
  end
  log(" Album, Success, Skipped, Size: " + album + "," + successCount.to_s + "," + skippedCount.to_s + "," + totalSize.to_s)
end

def process_file(path, f, album)
  if File.directory?(path + "/" + f)
    return "dir", 0
  end

  ext = File.extname(f).downcase
  mimeType = 'application/octet-stream'
  if (ext == ".jpg")
    mimeType = "image/jpeg"
  elsif (ext == ".gif")
    log(" skipped: " + f)
    return "skipped", 0
    mimeType = "image/gif"
  end

  log(" uploading: " + album + "_" + f + ". " + File.mtime(path + "/" + f).strftime("%FT%T.%L%:z"))
  success = insert_file($client, $drive, $googlePhotos,path + "/" + f, album + "_" + f, mimeType)

  if success
    fileSize = File.size(path + "/" + f).to_f
    return "success", fileSize
  else
    return "error", 0
  end
end

if __FILE__ == $0
  $client, $drive, $googlePhotos = setup()
  #insert_file(client, drive)
  process_albums()
end