# Copyright (C) 2012 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
require 'rubygems'
require 'logger'
require 'open3'
require 'pathname'


$smugline_prog = "~/dev/smugline/smugline.py"
$smugline_auth = "--api-key=CkrdQM5RSPxYcAaMlWT5F0kuzYUAaHSv --email=ariscris@gmail.com --password=arisgo"

def log(s)
  $al.info(s)
  puts(s)
end

def process_albums()
  log_file = File.open('albums.log', 'a+')
  log_file.sync = true
  $al = Logger.new(log_file)

  cmd = $smugline_prog + " list " + $smugline_auth

  log("Executing: " + cmd)
  list, st = Open3.capture2(cmd)

  skippedFirst = false
  skipUntil = "" #Palarong Pinoy '09"
  list.each_line do |line|
    album = line.strip()
    if skipUntil != ""
      if album == skipUntil
        skipUntil = ""
      else
        log("skipping " + album)
        next
      end
    end
    if skippedFirst
      process_album(album) 
      #break
    else
      skippedFirst = true
    end
  end
end

def process_album(album)
  if album == "iphone" || album == "PhotoSync" || album == "Photosync 2" || album == "Camera Awesome Archive" ||
    album == "My Gehls Archive" || album.include?("Months Old") || album.include?("years old") || album == "Videos"
    return
  end

  path = "/Volumes/My Passport/Smugmug/" + album
  log(Time.now.strftime("%d/%m/%Y %H:%M") + " Processing album: " + album)

  Dir.mkdir path unless File.exists?(path)
  Pathname.new(path).children.each { |p| p.unlink }

  cmd = $smugline_prog + " download '" + album + "' --to='" + path + "' " + $smugline_auth

  #log(" Executing: " + cmd)
  list, st = Open3.capture2(cmd)

  # successCount = 0
  # skippedCount = 0
  # totalSize = 0
  # Dir.entries(path).select do |f| 
  #   ret, size = process_file(path, f, album)
  #   if ret == "success"
  #     successCount = successCount + 1
  #     totalSize = totalSize + size 
  #   elsif ret == "skipped"
  #     skippedCount = skippedCount + 1
  #   end
  # end
  # log(" Album, Success, Skipped, Size: " + album + "," + successCount.to_s + "," + skippedCount.to_s + "," + totalSize.to_s)
end

if __FILE__ == $0
  process_albums()
end